import os
import sys
import subprocess
import argparse

class Install_Exec():
    """Custom install setup to help run shell commands (outside shell) before
    installation"""

    def __init__(self, out_folder, bin_path):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.bin_path = bin_path
        if not os.path.exists(self.bin_path):
            os.mkdir(self.bin_path)
        self.out_folder = out_folder

    def install_kma(self):
        command1="git clone https://bitbucket.org/genomicepidemiology/kma/src/master/ {}/kma".format(self.out_folder)
        process1=subprocess.Popen(command1, shell=True, cwd=self.out_folder)
        out, err = process1.communicate()
        command2="make"
        make_command = "make"
        make_proc = subprocess.Popen(make_command, shell=False, cwd=self.out_folder+"/kma")
        stdout, stderr = make_proc.communicate()
        self.create_link(location=self.out_folder+"/kma/kma", program="kma")
        self.create_link(location=self.out_folder+"/kma/kma_index", program="kma_index")
        self.create_link(location=self.out_folder+"/kma/kma_shm", program="kma_shm")


    def install_entrez(self):
        command1 = 'curl ftp://ftp.ncbi.nlm.nih.gov/entrez/entrezdirect/edirect.tar.gz -o {}/edirect.tar.gz'.format(self.out_folder)
        process1=subprocess.Popen(command1, shell=True, cwd=self.out_folder)
        out, err = process1.communicate()
        command2="gunzip -c edirect.tar.gz | tar xf -"
        process2=subprocess.Popen(command2, shell=True, cwd=self.out_folder)
        out, err = process2.communicate()
        command3="rm {0}/edirect.tar.gz".format(self.out_folder)
        process3=subprocess.Popen(command3, shell=True, cwd=self.out_folder)
        out, err = process3.communicate()
        command4="{0}/edirect/setup.sh".format(self.out_folder)
        process4=subprocess.Popen(command4, shell=True, cwd=self.out_folder)
        out, err = process4.communicate()
        self.create_link(location=self.out_folder+"/edirect/esearch", program="esearch")
        self.create_link(location=self.out_folder+"/edirect/efetch", program="efetch")
        self.create_link(location=self.out_folder+"/edirect/edirect.pl", program="edirect.pl")


    def install_usearch(self):
        if sys.platform == "linux":
            command1 = "curl https://www.drive5.com/downloads/usearch11.0.667_i86linux32.gz -o {}/usearch11.0.gz".format(self.out_folder)
        elif sys.platform == "darwin":
            command1 = "curl https://www.drive5.com/downloads/usearch11.0.667_i86osx32.gz -o {}/usearch11.0.gz".format(self.out_folder)
        process1 = subprocess.Popen(command1, shell=True, cwd=self.out_folder)
        out, err = process1.communicate()
        command2 = "gunzip {}/usearch11.0.gz".format(self.out_folder)
        process2 = subprocess.Popen(command2, shell=True, cwd=self.out_folder)
        out, err = process2.communicate()
        command3 = "chmod 775 {}/usearch11.0".format(self.out_folder)
        process3 = subprocess.Popen(command3, shell=True, cwd=self.out_folder)
        out, err = process3.communicate()
        self.create_link(location=self.out_folder+"/usearch11.0", program="usearch")


    def create_link(self,location, program):
        command_link = "ln -s {0} {1}/{2}".format(location, self.bin_path, program)
        print(command_link)
        process_link = subprocess.Popen(command_link, shell=True)
        out, err = process_link.communicate()

def get_arguments():
    """Parse the arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-req', '--programs_required',  help="List of programs"+
                    "to be installed. Choose in a combination between 'kma',"+
                    " 'usearch', and 'edirect'; or 'all' to install all of them",
                    required=True, default='all')
    parser.add_argument('-out', '--output_folder',  help="Folder where to save programs", required=True)
    parser.add_argument('-bin', '--exec_folder', help="Folder containing symbolic links to the executables required")
    args = parser.parse_args()
    return args

def main():
    args = get_arguments()

    out_folder = os.path.abspath(args.output_folder)
    if args.exec_folder == None:
        exec_folder = out_folder
    else:
        exec_folder = os.path.abspath(args.exec_folder)
    installation = Install_Exec(out_folder=out_folder, bin_path=exec_folder)
    dict_programs = {"kma":installation.install_kma(),
                     "usearch":installation.install_usearch(),
                     "edirect":installation.install_entrez()}

    if args.programs_required == "all":
        install_programs = ['usearch', 'kma', 'edirect']
    elif args.programs_required != None:
        install_programs = "".join(args.programs_required.split())
        install_programs = install_programs.split(",")
    else:
        print("Not program specified")

    programs_option = ['usearch', 'kma', 'edirect']

    for program in install_programs:
        if program == "all":
            sys.exit("# Error: Please introduce 'all' or a combination of the lists: ['usearch', 'kma', 'edirect']")
        elif program not in programs_option:
            sys.exit("# Error: Please introduce 'all' or a combination of the lists: ['usearch', 'kma', 'edirect']")
        else:
            dict_programs[program]



################################################################################
#	MAIN
################################################################################
if __name__ == '__main__':
    main()
