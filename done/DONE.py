#!/usr/bin/env python3

import argparse
import sys, os, subprocess
from subprocess import Popen, PIPE
import time
import datetime
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import re
from Bio import Entrez
import random
import socket
import requests
#from clean_up_db import Clean_Up_Db
from .clean_up_db import Clean_Up_Db


################################################################################
#	FUNCTIONS
################################################################################
class DONE():

    def __init__(self, input_file, output_folder, bin_folder):
        self.input_file = os.path.abspath(input_file)
        self.out_folder = os.path.abspath(output_folder)
        self.format = format
        self.bin_path =  os.path.abspath(bin_folder)

    def download_db(self, format, restart, min, max, complete):
        """Get entries and download database"""
        start_time = time.time()
        # # Parse input file input dict
        databases = dict()
        with open(self.input_file, 'r') as f:
            for line in f:
                tmp = line.split("\t")
                databases[tmp[0]] = tmp[1]

        # List for text file with all GenBank files
        self.all_gb_list = list()
        self.all_gb = "%s/all_gb.txt"%(self.out_folder)

        # Make summary file
        os.system("mkdir -p %s/updates"%(self.out_folder))
        now = datetime.datetime.now()
        year = int(now.year)
        month = int(now.month)
        day = int(now.day)
        if restart == False:
           update = "%s/updates/update_%d_%d_%d.txt"%(self.out_folder, year, month, day)
        else:
           update = "%s/updates/restart_%d_%d_%d.txt"%(self.out_folder, year, month, day)

        up_list = list()
        up_list.append("Family\tNo. entries\tNo. new entries\tEntries deleted from NCBI")

        # Downlaod databases
        for db,term in sorted(databases.items()):

           term = term.rstrip()

           # Define database folder
           db_folder = "%s/%s"%(self.out_folder, db)
           os.system("mkdir -p %s"%(db_folder))

           # Download file with all accession number for each search term
           if min or max:
              term = term + " AND %d:%d[SLEN]"%(min, max)
           if complete == True:
              term = term + " AND \"complete genome\""

#           cmd = "{0}/edirect/esearch -db {1} -query '{2}' | {0}/edirect/efetch -db {1} -format acc ".format(self.bin_path, format, term)
           cmd = "{0}/esearch -db {1} -query '{2}' | {0}/efetch -db {1} -format acc ".format(self.bin_path, format, term)
           process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
           out, err = process.communicate()

           accessions = out.decode().split("\n")[:-1]
           print("# Number of entries identified for %s: %d"%(db, len(accessions)))

           # Compare to already downloaded accession numbers if not restart
           if restart == False and os.path.exists("%s/all_acc.txt"%(db_folder)):

              # Read all_acc.txt
              all_acc = open("%s/all_acc.txt"%(db_folder)).read().split('\n')
              if '' in all_acc:
                 all_acc.remove('')

              # Compare lists
              new_acc = list(set(accessions) - set(all_acc))

              # Check if an entry has been deleted from NCBI since last download
              wrong_acc = list(set(all_acc) - set(accessions))

              # Write to summary file
              if len(wrong_acc) != 0:
                 up_list.append("%s\t%d\t%d\t%s"%(db, len(accessions), len(new_acc), " ".join(wrong_acc)))
              else:
                 up_list.append("%s\t%d\t%d\tNone"%(db, len(accessions), len(new_acc)))


              if len(new_acc) == 0:
                 print("# Database for %s is up to date"%(db))
                 gb_file = '%s/all_%s.gb'%(db_folder, db)
                 self.all_gb_list.append(gb_file)
                 continue
              else:
                 gb_file = '%s/new_%s.gb'%(db_folder, db)
           else:
              new_acc = accessions
              gb_file = '%s/all_%s.gb'%(db_folder, db)
              up_list.append("%s\t%d\t%d\tNone"%(db, len(accessions), len(new_acc)))

              # Remove everything in db folder from previouse downloads
              os.system("rm -rf %s/*"%(db_folder))

           # Downlaad new entries
           print("# Number of entries to be download for %s: %d"%(db, len(new_acc)))

           # Download in subsets
           batchsize = 250
           downloads = list()
           for i in range(0, len(new_acc), batchsize):
              if (i+batchsize) > len(new_acc):
                 query = ','.join(new_acc[i:])
                 #batch = new_acc[i:(len(new_acc) - 1)] # the result might be shorter than batchsize at the end

                 # if len(batch) != 0:
                 #    download = " [ACCN] OR ".join(batch) + " [ACCN] OR " + new_acc[-1]
                 # else:
                 #    download = new_acc[-1]
              else:
                 query = ','.join(new_acc[i:(i + batchsize)])
                 #batch = new_acc[i:(i+batchsize - 1)] # the result might be shorter than batchsize at the end
                 #download = " [ACCN] OR ".join(batch) + " [ACCN] OR " + new_acc[(i+batchsize - 1)]

              if format == 'protein':
                 url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=%s&id=%s&rettype=gp&retmode=text"%(format, query)
                 #handle=Entrez.efetch(db=format,id='%s [ACCN]'%(download),rettype='gp')
                 #cmd = " esearch -db %s -query '%s [ACCN]' | efetch -format gp >> %s"%(format, download, gb_file)
              else:
                 url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=%s&id=%s&rettype=gb&retmode=text"%(format, query)
                 #handle=Entrez.efetch(db=format,id='%s [ACCN]'%(download),rettype='gb')
                 #cmd = " esearch -db %s -query '%s [ACCN]' | efetch -format gb >> %s"%(format, download, gb_file)

              #process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
              #out, err = process.communicate()
              tries = 10
              for i in range(tries):
                 try:
                    r = requests.get(url)
                 except requests.exceptions.ConnectionError as e:
                    print(e)
                    if i < tries - 1: # i is zero indexed
                       time.sleep(20)
                       continue
                    else:
                       raise
                 break

              downloads.append(r.content.decode())
              time.sleep(10)

           with open(gb_file, 'w') as f:
              f.write(''.join(downloads))

           # Merge with old file if not restart
           if restart == False and os.path.exists("%s/all_acc.txt"%(db_folder)):
              os.system("cat %s >> %s/all_%s.gb "%(gb_file, db_folder, db))
              os.system("rm %s"%(gb_file))
              gb_file = '%s/all_%s.gb'%(db_folder, db)

           # Make all_acc.txt file to compare with next download
           with open("%s/all_acc.txt"%(db_folder), "w") as txt:
              txt.write("\n".join(accessions))
              txt.write("\n")

           # Add gb file to text file with all databases
           self.all_gb_list.append(gb_file)

        # Write all_gb file
        with open(self.all_gb, "w") as gb:
           gb.write("\n".join(self.all_gb_list))

        # Write summary file
        with open(update, "w") as up:
           up.write("\n".join(up_list))

        print("# time used on download: " + str(round(time.time()- start_time, 3)) + " s")

    def clean_db_overview(self, clean_up, ex_ids=None, clean_up_db=None,
                            threshold=None, db_files=None, max_dist=None,
                            cut_off=None):
        "Clean up database step"

        start_time = time.time()
        # Run clean-up
        db_out = "%s/database"%(self.out_folder)
        old_db = "%s/old_database"%(self.out_folder)

        if os.path.exists(db_out) == True:

           # Delete previouse old database if it exists
           if os.path.exists(old_db) == True:
              os.system("rm -r %s"%(old_db))

           # Make folder to save old db
           os.system("mkdir %s"%(old_db))

           # Move old database
           os.system("mv %s/* %s"%(db_out, old_db))
        else:
           os.system("mkdir %s"%(db_out))

        if clean_up == True:

           clean_run = Clean_Up_Db(input_file=self.all_gb,
                                    bad_seq_path=clean_up_db,
                                    out_folder=self.out_folder,
                                    bin_folder=self.bin_path)
           clean_run.index_excluded_db(db_files=db_files)
           clean_run.memory_database(option="load")
           clean_run.clean_databases(exclusion_id_file=ex_ids,
                                    threshold=threshold,max_dist=max_dist,
                                    obs_cut_off=cut_off)
           clean_run.write_results()
           clean_run.memory_database(option="destroy")

           print("# time used on clean up: " + str(round(time.time()- start_time, 3)) + " s")
        else:
           # Database overview file
           db_overview = list()
           db_overview_header = "Accession no.\tOrganism\tTaxanomy id\tFamily"
           db_overview.append(db_overview_header)

           for database_file in self.all_gb_list:
              entries = dict()
              name = database_file.split("/")[-1].split("_")
              out_name = name[1].split(".")[0]

              for record in SeqIO.parse(database_file, "genbank"):
                 acc = record.id
                 seq = record.seq
                 entry_length = len(record.seq)
                 source = str(record.annotations["source"])
                 features = record.features
                 acc_clean = acc.split('.')[0]
                 taxid = ''
                 for feature in features:
                    qual = feature.qualifiers
                    if 'db_xref' in qual:
                       tmp = qual['db_xref']
                       for line in tmp:
                          m = re.search('taxon:(\d+)', line)
                          if m != None:
                             taxid = str(m.group(0)).split(":")[1]
                 new_name = "%s:%s"%(acc_clean, source.replace(' ', '_'))
                 entries[new_name] = seq

                 db_overview.append("%s\t%s\t%s\t%s"%(acc_clean, source, taxid, out_name))

              file = "%s/%s_db.fsa"%(db_out, out_name)
              with open(file, "w") as f1:
                 for entry,sequence in entries.items():
                    f1.write(">%s\n"%(entry))
                    f1.write("%s\n"%(sequence))

           with open('%s/database_overview.txt'%(db_out), 'w') as o:
              o.write('\n'.join(db_overview))




def get_arguments():
    """Parse the arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-input', '--input_file',
                        help="File with names of databases and search terms")
    parser.add_argument('-out', '--output_folder',
                        help="Folder to store the output", default='download')
    parser.add_argument('-bin', '--exec_folder',
                        help="""Folder with required/optional executables
                        /symbolic links to executables""", required=True)
    parser.add_argument('-ex_ids', '--ex_ids_file',
                        help="file with ids to be excluded", default=None)
    parser.add_argument('-c_db', '--clean_up_db',
                        help="""Name of database with sequences to remove
                        /contamination database""", default=None)
    parser.add_argument('-f', '--format',
                        help="""Choose format to download database in (protein
                        or nucleotide), default = 'protein'""",
                        default='protein')
    parser.add_argument('-r', '--restart', action="store_true",
                        help="""Restart the download to download all entries
                        again, default = False""", default=False)
    parser.add_argument('-c', '--clean_up', action="store_true",
                        help="""Clean-up the downloaded database using
                        'clean_up_db.py', default = False""", default=False)
    parser.add_argument('-min', '--min_length',
                        help="""Minimum length requirment for all databases
                        entries""")
    parser.add_argument('-max', '--max_length',
                        help="""Maximum length requirment for all databases
                        entries""")
    parser.add_argument('-t', '--threshold',
                        help="""Minimum threshold for removing sequences
                        matching to entries in the contaminations database,
                        default = 0.6""", default=0.6)
    parser.add_argument('-complete', '--complete_genomes', action="store_true",
                        help="""Download complete genomes and not all entries,
                        default = False""", default=False)
    parser.add_argument('-c_files', '--clean_up_files',
                        help="""list of name/s of database with sequences to
                        remove/contamination database. If the database is
                        already indexed and you do not want to index it again,
                        introduce the name of the indexed database, without any
                        of the KMA extensions (.seq.b,.comp.b,.name,.lenght.b).
                        It will use that indexed database""", default="all")
    parser.add_argument('-md', '--maxdist',
                        help="""Maximum distance (complete match at 0 and 1 for
                        completly different) to other sequence within the
                        subdatabase, default = 0.5""", default=0.5)
    parser.add_argument('-cutoff', '--obs_cut_off',
                        help="""Cut-off value for how many entries with high
                        similarity needed to be found in order to keep the
                        entry""", default=1)

    args = parser.parse_args()
    return args


def main():
    args = get_arguments()
    # Check if input file with taxids is provided
    if args.input_file != None:
       input = args.input_file
       print("# Input file: " + input )
    else:
       sys.stderr.write("Please specify an input file!\n")
       sys.exit(2)

    # Make output folder if it doesn't exist
    try:
       out_folder = args.output_folder
       os.system("mkdir -p %s"%(out_folder))
       print("# Output folder: " + str(out_folder))
    except:
       sys.stderr.write("Problem with output folder (option -out)!\n")
       sys.exit(2)

    run = DONE(input_file=input, output_folder=out_folder, bin_folder=args.exec_folder)

    # Check if correct output format is provided
    if args.format == "protein" or args.format == "nucleotide":
       format = args.format
       print("# Database format: " + format )
    else:
       sys.stderr.write("Wrong database format (protein or nucleotide)!\n")
       sys.exit(2)

    restart = args.restart
    if restart == True:
       print("# The download is restarted and all entries will be included" )
    else:
       print("# Only new entries since last download will be included" )

    # Check min and max values
    if args.min_length and args.max_length:
       min = int(args.min_length)
       max = int(args.max_length)
    elif args.min_length:
       min = int(args.min_length)
       max = 100000000000
    elif args.max_length:
       min = 0
       max = int(args.max_length)
    else:
       min = 0
       max = 0

    # Check if complete genomes is to be downlaoded
    complete = args.complete_genomes
    if complete == True:
       print("# The download is set to only download complete genomes by adding \"AND 'complete genome'\" to the NCBI search term" )
    else:
       print("# The download is not exclusively for complete genomes" )

    run.download_db(format, restart, min, max, complete)


    # Check if clean-up is turned on
    clean_up = args.clean_up
    if clean_up == True:
       print("# The downlaoded databases are cleaned" )

       # Check if input file with IDs to be excluded is provided
       if args.ex_ids_file != None:
          ex_ids = args.ex_ids_file
          print("# File with IDs to be excluded: " + ex_ids )
       else:
          ex_ids = ""
          print("# No file with IDs to be excluded is provided for the clean-up")

       # Check if input file with motifs to be excluded is provided
       threshold = float(args.threshold)
       if args.clean_up_db != None:
          clean_up_db = args.clean_up_db
          print("# Path to database with sequences to remove: " + clean_up_db )
          print("# Threshold for excluding sequence hits in clean-up: " + str(threshold))
       else:
          clean_up_db = ""
          print("# No database with sequences to remove is provided for the clean-up")
       if args.clean_up_files == "all":
          db_files = []
          for file in os.listdir(clean_up_db):
              db_files.append(os.path.join(os.path.abspath(clean_up_db),file))
       elif args.clean_up_files != None:
           string_c_files = "".join(args.clean_up_files.split())
           string_c_list = string_c_files.split(",")
           db_files = []
           for file in string_c_list:
               if os.path.isfile(os.path.join(os.path.abspath(clean_up_db),file)):
                   db_files.append(os.path.join(os.path.abspath(clean_up_db),file))
               else:
                   print("# The database name for sequences to remove does not exist: "+ str(file))
       else:
           print("# No database for clean up was provided")
       max_dist=float(args.maxdist)
       print("The maximum distance allowed is {}".format(max_dist))
       run.clean_db_overview(clean_up=clean_up, ex_ids=ex_ids,
                                clean_up_db=clean_up_db, threshold=threshold,
                                db_files=db_files, max_dist=max_dist, cut_off=int(args.obs_cut_off))
    else:
        print("# The downlaoded databases are not cleaned" )
        run.clean_db_overview(clean_up=clean_up)



################################################################################
#	MAIN
################################################################################
if __name__ == '__main__':
    main()
