#!/usr/bin/env python3

################################################################################
# Import libraries
################################################################################
import sys
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import csv
import subprocess
import datetime
import re
import gzip
from subprocess import Popen, PIPE
import argparse

################################################################################
# CLASSES:
################################################################################
class Clean_Up_Db():

    def __init__(self, input_file, bad_seq_path, out_folder, bin_folder):
        self.bin_path = bin_folder
        self.path_to_usearch = self.bin_path+"/usearch"
        self.bad_seq_path = bad_seq_path
        self.out_folder = os.path.abspath(out_folder)
        self.get_database_files(os.path.abspath(input_file))
        self.initiate_files()


    def initiate_files(self):
        # Database overview file
        self.db_overview = list()
        db_overview_header = "Accession no.\tOrganism\tTaxanomy id\tFamily"
        self.db_overview.append(db_overview_header)

        # Summary file
        self.summary_list = list()
        summary_header_string = "Database file\tNumber gb entries\tRecords expelled\tNumber unique records\tNew name"
        self.summary_list.append(summary_header_string)

        # Fasta list file
        self.fasta_list = list()


    def index_excluded_db(self, db_files):
        if len(db_files) == 1 and os.path.exists(db_files[0]+'.seq.b') and os.path.exists(db_files[0]+'.length.b') and os.path.exists(db_files[0]+'.comp.b') and os.path.exists(db_files[0]+'.name'):
            print("# The database(s) with sequences to clean is already indexed.")
            self.clean_db_name = os.path.splitext(os.path.basename(db_files[0]))[0]
        else:
            print("# The database(s) with sequences to clean are being indexed.")
            db_files = ' '.join(db_files)
            cmd = "cat {0} > {1}/clean_up_databases.fsa".format(db_files, self.bad_seq_path)
            process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
            out, err = process.communicate()

            self.clean_db_name = "clean_up_db"

            cmd = "{0}/kma_index -i {1}/clean_up_databases.fsa -o {1}/{2}".format(self.bin_path, self.bad_seq_path,self.clean_db_name)
            process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
            out, err = process.communicate()


    def memory_database(self, option):
        if option == "load":
            print("# Loading database into shared memory")
            cmd = "{0}/kma_shm -t_db {1}/{2}".format(self.bin_path, self.bad_seq_path, self.clean_db_name)
            process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
            out, err = process.communicate()
        elif option == "destroy":
            print("# Destroy database of shared memory")
            cmd = "{0}/kma_shm -t_db {1} -destroy".format(self.bin_path, self.bad_seq_path)
            process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
            out, err = process.communicate()


    def get_database_files(self, genbank_entries_list_file):
        print("# Starting clean-up")
        self.database_file_list=[]
        genbank_files = open(genbank_entries_list_file, "rU")
        motif_reader= csv.reader(genbank_files)
        for row in motif_reader:
            self.database_file_list.append(row[0])
        genbank_files.close()

    def write_results(self):
        # Write up results
        print("# Write files")

        with open(self.out_folder+"/database/fasta_list.txt", "w") as f_list:
           f_list.write('\n'.join(self.fasta_list))

        with open(self.out_folder+"/database/summary.txt", "w") as f_sum:
           f_sum.write('\n'.join(self.summary_list))

        with open(self.out_folder+'/database/database_overview.txt', 'w') as o:
           o.write('\n'.join(self.db_overview))

        cmd = "cat {0} > {1}/database/all_databases.fsa".format(" ".join(self.fasta_list), self.out_folder)
        process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()

    def make_set_unique(self,outprefix, database_file, number_gb_entries):
        file_to_make_unique = "{0}_to_make_unique.fsa".format(self.out_folder+"/"+outprefix)
        with open(file_to_make_unique, "w") as f1:
            for entry,seq in self.entries.items():
                f1.write(">%s\n"%(entry))
                f1.write("%s\n"%(seq))

        unique_file = "{0}_unique.fsa".format(self.out_folder+"/"+outprefix)

        #cmd = "%s -fastx_uniques %s -fastaout %s"%(path_to_usearch, file_to_make_unique, unique_file)
        cmd = "{0} -fastx_uniques /{1} -fastaout {2}".format(self.path_to_usearch, file_to_make_unique, unique_file)
        process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()


        unique_records = list(SeqIO.parse(unique_file, "fasta"))
        number_unique_records = str(len(unique_records))
        os.remove(file_to_make_unique)

        now = datetime.datetime.now()
        new_name = self.out_folder+"/database/{0}_{1}_{2}_{3}_{4}.fsa".format(outprefix, str(number_unique_records), now.year, now.month, now.day)

        print("# Unique records: " +str(len(unique_records)))

        self.fasta_list.append("%s"%( new_name))
        expelled_print_string = "%s\t%s\t%s\t%s\t%s"%(database_file, str(number_gb_entries), str(len(self.expel_count_list)), str(len(unique_records)), new_name)
        self.summary_list.append("%s"%(expelled_print_string))

        return unique_file, unique_records

    def remove_bad_seq(self, outprefix):
        kma_input_file = self.out_folder+"/"+outprefix+"_kma_input.fsa"
        with open(kma_input_file, "w") as f1:
            for entry,seq in self.entries.items():
                f1.write(">%s\n"%(entry))
                f1.write("%s\n"%(seq))
        f1.close()

        cmd = "{0}/kma -i {1} -t_db {2}/{4} -shm -o {3}/tmp".format(self.bin_path, kma_input_file, self.bad_seq_path, self.out_folder, self.clean_db_name)
        process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()

        os.remove(kma_input_file)

        # Collect length of template hits
        result = dict()
        with open(self.out_folder+"/tmp.res", "r") as res:
            header = res.readline()
            for line in res:
                elem = line.split("\t")
                result[elem[0].rstrip()] = float(elem[3])

        # Remove entires that has a hit to the database with a high coverage
        with gzip.open(self.out_folder+"/tmp.frag.gz", "rb") as frag:
            for line in frag:
                line = line.decode().rstrip()
                tmp = line.split("\t")
                if len(tmp) != 7:
                    continue
                score = int(tmp[2])
                temp_header = tmp[5]
                query_header = tmp[6]

                if (query_header in self.entries) and (score/result[temp_header] > self.threshold):
                    self.entries.pop(query_header)
                    self.expel_count_list.append("{0}\t{1}".format(query_header,temp_header))

        for file in os.listdir(self.out_folder):
            if os.path.isfile(os.path.join(self.out_folder,file)) and file.startswith("tmp."):
                os.remove(os.path.join(self.out_folder,file))

        records_after_sequence_removal = len(self.entries)
        print("# Records after bad sequence removal: %d"%(records_after_sequence_removal))

    def remove_excl_seq(self, outprefix, exclusion_id_file):
        # Step 2 remove inappropriate entries based on list of IDs
        exclusion_id_list = []
        exclusion_ids_to_expel = open(exclusion_id_file, "rU")
        motif_reader= csv.reader(exclusion_ids_to_expel)
        for row in motif_reader:
            exclusion_id_list.append(row[0])
        exclusion_ids_to_expel.close()

        # Delete bad entries
        if len(exclusion_id_list) != 0:
            for seq_name in list(self.entries):
                for id in exclusion_id_list:
                    if id in seq_name:
                        self.entries.pop(seq_name)
                        self.expel_count_list.append(seq_name)
                        break

        records_after_id_purge = len(self.entries)
        print("# Records after id purge: %d"%(records_after_id_purge))

    def remove_outliers_kma(self, outprefix, unique_file, unique_records):
        """Remove outliers entries with kma"""
        index_cmd = "{0}/kma_index -i {1} -NI -Sparse -".format(self.bin_path,unique_file)
        process = subprocess.Popen(index_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()

        dist_file = self.out_folder+"/%s_dist_file.txt"%(outprefix)
        dist_cmd = "{0}/kma dist -t_db {1} -d 4 > {2}".format(self.bin_path, unique_file, dist_file)
        process = subprocess.Popen(dist_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()
        observed_entries_counts = dict()

        with open(dist_file, 'r') as f:
            for line in f:
                line = line.rstrip().split("\t")
                cover_line = list(map(float, line[1:]))
                if len(line) == 1:
                    continue
                else:
                    occured=sum(i > self.max_dist for i in cover_line)
                    observed_entries_counts[line[0]]=occured

        # Save final file
        now = datetime.datetime.now()
        new_name = self.out_folder+"/database/%s_%s_%s_%s_%s.fsa"%(outprefix, str(len((unique_records))), now.year, now.month, now.day)
        final_entries = list()
        outliers = 0
        with open(new_name, "w") as f1:
            for record in unique_records:
                entry = record.id
                if (entry in observed_entries_counts.keys() and observed_entries_counts[entry] > self.obs_cut_off):
                    final_entries.append(entry)
                    f1.write(">%s\n"%(entry))
                    f1.write("%s\n"%(self.entries[entry]))
                else:
                    outliers+=1
                    self.expel_count_list.append("%s\t%s"%(entry, 'outlier'))

        print("# Number of record outliers deleted: "+str(outliers))
        os.remove(unique_file)
        os.remove(dist_file)
        os.remove(unique_file+".comp.b")
        os.remove(unique_file+".length.b")
        os.remove(unique_file+".name")
        os.remove(unique_file+".seq.b")

    def write_excluded(self, outprefix):
        with open(self.out_folder+"/database/%s_excluded_entries.txt"%(outprefix), 'w') as f:
            if len(self.expel_count_list) != 0:
                f.write('\n'.join(self.expel_count_list))
            else:
                f.write("None\n")


    def remove_outliers_usearch(self, outprefix, unique_file, unique_records):
        """Remove outlier entries with usearch"""
        dist_file = "%s_dist_file.txt"%(outprefix)
        cmd = "%s -calc_distmx %s -tabbedout %s -maxdist %s"%(self.path_to_usearch, unique_file, dist_file, self.max_dist)
        process = subprocess.Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()

        observed_entries = list()
        with open(dist_file, 'r') as f:
            for line in f:
                tmp = line.split("\t")
                if tmp[0] != tmp[1]:
                    observed_entries.append(tmp[0])

        # count occurences
        observed_entries_counts = dict((x,observed_entries.count(x)) for x in set(observed_entries))

        # Save final file
        now = datetime.datetime.now()
        new_name = self.outfolder+"/%s_%s_%s_%s_%s.fsa"%(outprefix, str(len((unique_records))), now.year, now.month, now.day)
        final_entries = list()
        with open(new_name, "w") as f1:
            for record in unique_records:
                entry = record.id
                if (entry in observed_entries_counts.keys() and observed_entries_counts[entry] > self.obs_cut_off):
                    final_entries.append(entry)
                    f1.write(">%s\n"%(entry))
                    f1.write("%s\n"%(self.entries[entry]))
                else:
                    self.expel_count_list.append("%s\t%s"%(entry, 'outlier'))

        os.remove(unique_file)
        os.remove(dist_file)

    def clean_databases(self, exclusion_id_file, threshold, max_dist, obs_cut_off):
        self.obs_cut_off = obs_cut_off
        self.threshold = threshold
        self.max_dist = max_dist*100
        for database_file in self.database_file_list:
            self.entries = dict()
            gb_file_pieces=database_file.replace("_",".").split(".")
            number_gb_entries=gb_file_pieces[1]
            self.expel_count_list=[]
            paths = database_file.split("/")
            pieces = paths[-1].split("_")
            outprefix = pieces[1].split(".")[0]
            for record in SeqIO.parse(database_file, "genbank"):
                accno = record.id
                sequence = record.seq
                length_entry = len(record.seq)
                source = str(record.annotations["source"])
                features = record.features
                accession_number_pieces=accno.split('.')
                accession_number_clean= accession_number_pieces[0]
                taxid = ''
                for feature in features:
                    qual = feature.qualifiers
                    if 'db_xref' in qual:
                        tmp = qual['db_xref']
                        for line in tmp:
                            m = re.search('taxon:(\d+)', line)
                            if m is not None:
                                taxid = str(m.group(0)).split(":")[1]
                new_name = accession_number_clean+':'+source.replace(' ', '_')
                new_name_80max= new_name[0:80]
                self.entries[new_name_80max] = sequence

                self.db_overview.append("{0}\t{1}\t{2}\t{3}".format(accession_number_clean, source, taxid, outprefix))

            starting_number_records = len(self.entries)
            print("# Starting number of records: %d"%(starting_number_records))

            if exclusion_id_file != None and exclusion_id_file != "":
                self.remove_excl_seq(outprefix, exclusion_id_file)
            if self.bad_seq_path != None and self.bad_seq_path != "":
                self.remove_bad_seq(outprefix)
            unique_file, unique_records = self.make_set_unique(outprefix,database_file, number_gb_entries)
            self.remove_outliers_kma(outprefix, unique_file, unique_records)
            self.write_excluded(outprefix)

################################################################################
# FUNCTIONS:
################################################################################
def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input_file',  help="Genbank entries list file")
    parser.add_argument('-o', '--output_folder',  help="folder to store the output", default='download')
    parser.add_argument('-ex_ids', '--ex_ids_file',  help="file with ids to be excluded", default=None)
    parser.add_argument('-bin', '--exec_folder', help="folder with required/optional executables/symbolic links to executables", required=True)
    parser.add_argument('-c_db', '--clean_up_db', help="Name of database with sequences to remove/contamination database", default=None)
    parser.add_argument('-t', '--threshold',  help="minimum threshold for removing sequences matching to entries in the contaminations database, default = 0.6", default = 0.6)
    parser.add_argument('-md', '--maxdist',  help="maximum distance (complete match at 0 and 1 for completly different) to other sequence within the subdatabase, default = 0.5", default = 0.5)
    parser.add_argument('-cutoff', '--obs_cut_off', help="Cut off of observed entries", default=1)
    parser.add_argument('-c_files', '--clean_up_files',
                        help="""list of name/s of database with sequences to remove/contamination
                        database. If the database is already indexed and you do not want to index it again,
                        introduce the name of the indexed database, without any of the KMA extensions
                        (.seq.b,.comp.b,.name,.lenght.b). It will use that indexed database""", default="all")
    args = parser.parse_args()
    return args


def main():

    args = parse_arguments()
    threshold = float(args.threshold)
    if args.clean_up_db != None:
        clean_up_db = args.clean_up_db
        print("# Path to database with sequences to remove: " + clean_up_db )
        print("# Threshold for excluding sequence hits in clean-up: " + str(threshold))
    else:
        clean_up_db = ""
        print("# No database with sequences to remove is provided for the clean-up")

    if args.clean_up_files == "all":
        db_files = []
        for file in os.listdir(clean_up_db):
            if file != "clean_up_databases.fsa" and not file.endswith(".b") and not file.endswith(".name"):
                db_files.append(os.path.join(os.path.abspath(clean_up_db),file))
    elif args.clean_up_files != None:
        string_c_files = "".join(args.clean_up_files.split())
        string_c_list = string_c_files.split(",")
        db_files = []
        for file in string_c_list:
            db_files.append(os.path.join(os.path.abspath(clean_up_db),file))
    else:
        print("# No database for clean up was provided")
    if os.path.isdir(args.output_folder+"/database"):
        pass
    else:
        os.mkdir(args.output_folder+"/database")
    clean_run = Clean_Up_Db(input_file=args.input_file,
                            bad_seq_path=args.clean_up_db,
                            out_folder=args.output_folder,
                            bin_folder=args.exec_folder)
    print(db_files)
    clean_run.index_excluded_db(db_files=db_files)
    clean_run.memory_database(option="load")
    clean_run.clean_databases(exclusion_id_file=args.ex_ids_file,
                                threshold=args.threshold, max_dist=float(args.maxdist),
                                obs_cut_off=int(args.obs_cut_off))
    clean_run.write_results()
    clean_run.memory_database(option="destroy")

################################################################################
#  MAIN
################################################################################
if __name__ == '__main__':
    main()
