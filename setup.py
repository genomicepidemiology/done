#!/usr/bin/env python
import subprocess, os, sys
from setuptools import setup, find_packages
from setuptools.command.install import install

with open("README.md", 'r') as f:
    long_description = f.read()

with open("done/__init__.py", 'r') as f:
    for l in f:
        if l.startswith('__version__'):
            version = l.split('=')[1].strip().strip('"')

setup(
    name="DONE",
    version=version,
    author="Alfred Ferrer Florensa",
    author_email="alff@food.dtu.dk",
    description="Download Of NCBI Entries",
    long_description=long_description,
    license="Apache License, Version 2.0",
    url="https://bitbucket.org/genomicepidemiology/done.git",
    packages=find_packages(),
    entry_points={'console_scripts': ['DONE = done.DONE:main',
                                      'DONE_requirements = done.install_requirements:main']},
    dependency_links=["https://bitbucket.org/genomicepidemiology/kma/src/master/"],
    include_package_data=True,
    install_requires=['argparse','datetime','biopython','requests',],
    zip_safe=False,
    setup_requires=["wheel"],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
