===================
DONE
===================

This project documents DONE service


Documentation
=============

## What is it?

DONE is an automated Download Of NCBI Entries module that further includes a virus database
clean-up script and examples of use for a clean family specific virus database.

## Content of the repository
- done                       - Folder containing the python scripts of the module
    - install_requirements.py  - Python script responsible of the installing the third parties software
    - clean_up_db.py           - Python script responsible of the clean-up of the databases
    - DONE.py                  - Python script responsible of the downloading of the databases.
- data                       - Folder containing data related to the module
    - clean_up_databases     - Folder containing fasta files for cleaning up virus databases.
        - genes.fsa
        - plasmid_db.fsa
        - rRNA_set_2Aug17.fsa
        - satellite_alpha_2_75.fsa
    - input.txt                     - Example of input file for the virus database
    - input_complete.txt            - Example of input file for the virus database
                                   with length preferences for complete genome download

## Installation

### Setting up DONE
DONE needs Python3.5 or bigger.
```bash
# Go to wanted location for DONE
cd /path/to/some/dir
# Clone and enter the DONE directory
git clone https://bitbucket.org/genomicepidemiology/done.git
cd done
# Install package
python3 -m pip install .
# or
pip install .
# Try if it has been installed by typing:
DONE -h
```
### Installing third party software
In order for the module to work correctly, it requires a folder /bin in the main directory. This folder bin has to contain the executables "kma", "kma_index", "kma_shm", "usearch" and "esearch" (or symbolic links).
This can be done manually or by running the executable DONE_requirements:
```bash
# Run the automated install requirements code
DONE_requirements -h
usage: DONE_requirements [-h] -req PROGRAMS_REQUIRED -out OUTPUT_FOLDER
                         [-bin EXEC_FOLDER]

optional arguments:
  -h, --help            show this help message and exit
  -req PROGRAMS_REQUIRED, --programs_required PROGRAMS_REQUIRED
                        List of programsto be installed. Choose in a
                        combination between 'kma', 'usearch', and 'edirect';
                        or 'all' to install all of them
  -out OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
                        Folder where to save programs
  -bin EXEC_FOLDER, --exec_folder EXEC_FOLDER
                        Folder containing symbolic links to the executables
                        required

# Example
DONE_requirements -req all -out /path/to/out_folder -bin /path/to/bin_folder
```
Notice that the program DONE requires a folder with the symbolic links or executables required. Because of that, if you already have the software in your computer and do not want to install it again, you just have to create a folder containing the next symbolic links/executables (and with these names):

1. From kma: "kma","kma_shm", "kma_index"
2. From edirect: "edirect.pl","efetch","esearch"
3. From usearch: "usearch"

## Usage

The program can be invoked with the -h option to get help and more information
of the service.

```bash
usage: DONE [-h] [-input INPUT_FILE] [-out OUTPUT_FOLDER] -bin EXEC_FOLDER
            [-ex_ids EX_IDS_FILE] [-c_db CLEAN_UP_DB] [-f FORMAT] [-r] [-c]
            [-min MIN_LENGTH] [-max MAX_LENGTH] [-t THRESHOLD] [-complete]
            [-c_files CLEAN_UP_FILES]

optional arguments:
  -h, --help            show this help message and exit
  -input INPUT_FILE, --input_file INPUT_FILE
                        file with names of databases and search terms
  -out OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
                        folder to store the output
  -bin EXEC_FOLDER, --exec_folder EXEC_FOLDER
                        folder with required/optional executables/symbolic
                        links to executables
  -ex_ids EX_IDS_FILE, --ex_ids_file EX_IDS_FILE
                        file with ids to be excluded
  -c_db CLEAN_UP_DB, --clean_up_db CLEAN_UP_DB
                        name of database with sequences to
                        remove/contamination database
  -f FORMAT, --format FORMAT
                        choose format to download database in (protein or
                        nucleotide), default = 'protein'
  -r, --restart         restart the download to download all entries again,
                        default = False
  -c, --clean_up        clean-up the downloaded database using
                        'clean_up_db.py', default = False
  -min MIN_LENGTH, --min_length MIN_LENGTH
                        minimum length requirment for all databases entries
  -max MAX_LENGTH, --max_length MAX_LENGTH
                        maximum length requirment for all databases entries
  -t THRESHOLD, --threshold THRESHOLD
                        minimum threshold for removing sequences matching to
                        entries in the contaminations database, default = 0.6
  -complete, --complete_genomes
                        download complete genomes and not all entries, default
                        = False
  -c_files CLEAN_UP_FILES, --clean_up_files CLEAN_UP_FILES
                        list of name/s of database with sequences to
                        remove/contamination database. If the database is already indexed and you do not want to index it again, introduce the name of the indexed database, without any of the KMA extensions (.seq.b,.comp.b,.name,.lenght.b). It will use that indexed database. Default = all
  -md MAXDIST, --maxdist MAXDIST
                        Maximum distance (complete match at 0 and 1 for
                        completely different) to other sequence within the
                        subdatabase, default = 0.5
  -cutoff OBS_CUT_OFF, --obs_cut_off OBS_CUT_OFF
                        Cut-off value for how many entries with high
                        similarity needed to be found in order to keep the
                                              entry



```

#### Example of download of all nucleotide sequences
```bash
    DONE -input /path/to/input.txt \
                   -out /path/to/output/ \
                   -f 'nucleotide' -r -bin /path/to/bin_folder
```

#### Example of download of all nucleotide sequences with length requirment
```bash
    DONE -input /path/to/input.txt \
                   -out /path/to/output/ \
                   -f 'nucleotide' -r \
                   -min 50 -max 10000 -bin /path/to/bin_folder
```

#### Example of download of all nucleotide sequences with clean-up
```bash
    DONE -input /path/to/input.txt \
                   -out /path/to/output/ \
                   -f 'nucleotide' -r -c \
                   -c_files /path/to/clean_up_db -bin /path/to/bin_folder

## The Latest Version


The latest version can be found at
https://bitbucket.org/genomicepidemiology/done/overview

## Documentation


The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/done/overview.


License
=======

Copyright (c) 2017, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
